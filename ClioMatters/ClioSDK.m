//
//  ClioSDK.m
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ClioSDK.h"
#import "Matter.h"
#import "Note.h"

#define AUTHORIZATION_KEY @"Bearer Xzd7LAtiZZ6HBBjx0DVRqalqN8yjvXgzY5qaD15a"
#define MATTER_GET_URL @"https://app.goclio.com/api/v2/matters"
#define NOTE_FOR_MATTER_GET_URL @"https://app.goclio.com/api/v2/notes?regarding_type=Matter&regarding_id=%ld"
#define NOTE_FOR_MATTER_CREATE_URL @"https://app.goclio.com/api/v2/notes"
#define NOTE_FOR_MATTER_UPDATE_URL @"https://app.goclio.com/api/v2/notes/%ld"

@implementation ClioSDK


+ (void)fetchMatterWithCompletionBlock:(void(^)(NSArray*)) completionBlock {

    NSURL *url = [NSURL URLWithString:MATTER_GET_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:AUTHORIZATION_KEY forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError *error = nil;
             NSArray *result = [ClioSDK entitiesFromJSON:data forRoot:@"matters" withClassString:@"Matter" error:&error];
             if (!error && completionBlock) {
                 completionBlock(result);
                 return;
             }
         }
         if (completionBlock) {
             completionBlock(nil);
         }
     }];
    
}

+ (void)fetchNotesForMatter:(NSInteger)matterId withCompletionBlock:(void(^)(NSArray*)) completionBlock {
    
    NSString *path = [NSString stringWithFormat:NOTE_FOR_MATTER_GET_URL, matterId];
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:AUTHORIZATION_KEY forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError *error = nil;
             NSArray *result = [ClioSDK entitiesFromJSON:data forRoot:@"notes" withClassString:@"Note" error:&error];
             if (!error && completionBlock) {
                 completionBlock(result);
                 return;
             }
         }
         if (completionBlock) {
             completionBlock(nil);
         }
     }];
    
}

+ (void)createNote:(Note*)note forMatter:(Matter*)matter withCompletionBlock:(void(^)(NSArray*)) completionBlock {
    
    NSURL *url = [NSURL URLWithString:NOTE_FOR_MATTER_CREATE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    id contentBody = [ClioSDK toJSONStringWithKeys:@{@"note": @{@"subject":note.subject, @"regarding":@{@"type":@"Matter",@"id":@(matter.id)}}}];
    NSData *data = [contentBody dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    [request setValue:AUTHORIZATION_KEY forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%ld",request.HTTPBody.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"POST"];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError *error = nil;
             NSArray *result = [ClioSDK entitiesFromJSON:data forRoot:@"matters" withClassString:@"Matter" error:&error];
             if (!error && completionBlock) {
                 completionBlock(result);
                 return;
             }
         }
         if (completionBlock) {
             completionBlock(nil);
         }
     }];
    
}


+ (void)updateNote:(Note*)note withCompletionBlock:(void(^)(NSArray*)) completionBlock {
    
    NSString *path = [NSString stringWithFormat:NOTE_FOR_MATTER_UPDATE_URL, note.id];
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    id contentBody = [ClioSDK toJSONStringWithKeys:@{@"note": @{@"subject":note.subject, @"detail":note.detail}}];
    NSData *data = [contentBody dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    [request setValue:AUTHORIZATION_KEY forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%ld",request.HTTPBody.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"PUT"];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError *error = nil;
             NSArray *result = [ClioSDK entitiesFromJSON:data forRoot:@"matters" withClassString:@"Matter" error:&error];
             if (!error && completionBlock) {
                 completionBlock(result);
                 return;
             }
         }
         if (completionBlock) {
             completionBlock(nil);
         }
     }];
    
}

+ (NSArray *)entitiesFromJSON:(NSData *)objectNotation forRoot:(NSString*)root withClassString:(NSString*)classString error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *entities = [[NSMutableArray alloc] init];
    
    NSArray *results = [parsedObject valueForKey:root];
    NSLog(@"Count %lu", (unsigned long)results.count);
    
    for (NSDictionary *matterDic in results) {
        
        id myclass = [[NSClassFromString(classString) alloc] init];
        
        for (NSString *key in matterDic) {
            if ([myclass respondsToSelector:NSSelectorFromString(key)]) {
                [myclass setValue:[matterDic valueForKey:key] forKey:key];
            }
        }
        
        [entities addObject:myclass];
    }
    
    return entities;
}

+(NSString*)toJSONStringWithKeys:(NSDictionary*)dict
{
    NSData* jsonData = nil;
    NSError* jsonError = nil;
    
    @try {
        jsonData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&jsonError];
    }
    @catch (NSException *exception) {
        //this should not happen in properly design JSONModel
        //usually means there was no reverse transformer for a custom property
        NSLog(@"EXCEPTION: %@", exception.description);
        return nil;
    }
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

@end
