//
//  ClioSDK.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Matter;
@class Note;

@interface ClioSDK : NSObject

+ (void)fetchMatterWithCompletionBlock:(void(^)(NSArray*)) completionBlock;
+ (void)fetchNotesForMatter:(NSInteger)matterId withCompletionBlock:(void(^)(NSArray*)) completionBlock;
+ (void)createNote:(Note*)note forMatter:(Matter*)matter withCompletionBlock:(void(^)(NSArray*)) completionBlock;
+ (void)updateNote:(Note*)note withCompletionBlock:(void(^)(NSArray*)) completionBlock;

@end
