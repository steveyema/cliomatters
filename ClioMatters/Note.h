//
//  Note.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Note : NSObject

@property (nonatomic, assign) NSInteger id;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *created_at;
@property (nonatomic, strong) NSString *updated_at;
@property (nonatomic, strong) NSDictionary *regarding;

@end
