//
//  Matter.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Matter : NSObject

@property (nonatomic, assign) NSInteger id;
@property (nonatomic, strong) NSDictionary *client;
@property (nonatomic, strong) NSString *display_number;
@property (nonatomic, strong) NSString *Description;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *open_date;
@property (nonatomic, strong) NSString *close_date;
@property (nonatomic, strong) NSString *pending_date;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *client_reference;
@property (nonatomic, strong) NSString *responsible_attorney;
@property (nonatomic, strong) NSString *originating_attorney;
@property (nonatomic, strong) NSString *practice_area;
@property (nonatomic, assign) BOOL billable;
@property (nonatomic, strong) NSString *maildrop_address;
@property (nonatomic, strong) NSString *created_at;
@property (nonatomic, strong) NSString *updated_at;
@property (nonatomic, strong) NSArray *custom_field_values;
@property (nonatomic, strong) NSString *billing_method;
@property (nonatomic, assign) NSInteger group_id;
@property (nonatomic, strong) NSDictionary *permission;

@end
