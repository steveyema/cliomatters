//
//  NotesViewController.m
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "NotesViewController.h"
#import "NoteViewController.h"
#import "Note.h"
#import "ClioSDK.h"
#import "NoteTableViewCell.h"
#import "Utils.h"
#import "Matter.h"

@interface NotesViewController ()<UITableViewDataSource, UITableViewDelegate, NoteViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *notesTableView;

@property (nonatomic, strong) NSArray *notes;

@end

@implementation NotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notesTableView.delegate = self;
    self.notesTableView.dataSource = self;
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(addNote:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    
    [ClioSDK fetchNotesForMatter:self.matter.id withCompletionBlock:^(NSArray *results) {
        //
        if (results) {
            self.notes = results;
            [self.notesTableView reloadData];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addNote:(id)sender {
    Note *note = [[Note alloc] init];
    note.subject = @"test";
    [ClioSDK createNote:note forMatter:self.matter withCompletionBlock:^(NSArray *result) {
        if (!result) {
            [Utils showStandardAlertViewWithMessage:@"failed to create note" fromViewController:self];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Note *note = (Note *)self.notes[indexPath.row];
    
    NoteViewController *noteViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController" bundle:[NSBundle mainBundle]];
    noteViewController.note = note;
    noteViewController.delegate = self;
    [self presentViewController:noteViewController animated:YES completion:nil];
    //    [self.navigationController pushViewController:noteViewController animated:YES];
}


#pragma  mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notes? self.notes.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NoteTableViewCell *cell = (NoteTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NoteTableViewCell"];
    
    if (!cell) {
        cell = [[NoteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NoteTableViewCell"];
    }
    
    Note *note = (Note*)self.notes[indexPath.row];
    
    if ([note.subject isKindOfClass:[NSString class]]) {
        cell.subjectLabel.text = note.subject;
    }
    if ([note.detail isKindOfClass:[NSString class]]) {
        cell.detailLabel.text = note.detail;
    }
    
    return cell;
}

#pragma mark - NoteViewControllerDelegate method

- (void)saveNote:(Note *)note {
    [ClioSDK updateNote:note withCompletionBlock:^(NSArray *result) {
        //
        if (!result) {
            [Utils showStandardAlertViewWithMessage:@"Failed to save the note!" fromViewController:self];
        }
        else {
            [self.notesTableView reloadData];
        }
    }];
}

@end
