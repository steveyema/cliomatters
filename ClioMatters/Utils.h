//
//  Utils.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+(void)showStandardAlertViewWithMessage:(NSString*)message fromViewController:(UIViewController*)viewController;

@end
