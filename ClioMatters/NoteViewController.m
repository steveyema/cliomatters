//
//  NoteViewController.m
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "NoteViewController.h"
#import "Note.h"

@interface NoteViewController ()
@property (weak, nonatomic) IBOutlet UITextField *subjectLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

- (IBAction)onSavePressed:(id)sender;
- (IBAction)onCancelPressed:(id)sender;

@end

@implementation NoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.subjectLabel.text = self.note.subject;
    self.descriptionLabel.text = self.note.detail;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSavePressed:(id)sender {
    
    self.note.subject = self.subjectLabel.text;
    self.note.detail = self.descriptionLabel.text;
    
    if ([self.delegate respondsToSelector:@selector(saveNote:)]) {
        [self.delegate saveNote:self.note];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onCancelPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
