//
//  MatterTableViewCell.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *displayNumberLabel;

@end
