//
//  NoteViewController.h
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Note;

@protocol NoteViewControllerDelegate <NSObject>

- (void)saveNote:(Note*)note;

@end

@interface NoteViewController : UIViewController

@property (nonatomic, strong) Note *note;

@property (nonatomic, weak) id<NoteViewControllerDelegate> delegate;

@end
