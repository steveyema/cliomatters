//
//  ViewController.m
//  ClioMatters
//
//  Created by Ye Ma on 2016-01-22.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "MattersViewController.h"
#import "ClioSDK.h"
#import "MatterTableViewCell.h"
#import "Matter.h"
#import "NotesViewController.h"

@interface MattersViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mattersTableView;

@property (nonatomic, strong) NSArray *matters;

@end

@implementation MattersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.mattersTableView.delegate = self;
    self.mattersTableView.dataSource = self;
    
    [ClioSDK fetchMatterWithCompletionBlock:^(NSArray *results) {
        if (results) {
            self.matters = results;
            [self.mattersTableView reloadData];
        }
        else {
            //TODO
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Matter *matter = (Matter *)self.matters[indexPath.row];

    NotesViewController *notesViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"NotesViewController"];
    notesViewController.matter = matter;
    [self.navigationController pushViewController:notesViewController animated:YES];
}


#pragma  mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.matters? self.matters.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MatterTableViewCell *cell = (MatterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MatterTableViewCell"];
    
    if (!cell) {
        cell = [[MatterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MatterTableViewCell"];
    }
    
    Matter *matter = (Matter*)self.matters[indexPath.row];
    
    cell.displayNumberLabel.text = matter.display_number;
    
    return cell;
}

@end
